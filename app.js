'use strict';

const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config({ path: './config/.env' });

const app = express();
const port = process.env.PORT || 3000;

app.use(express.static('public'));
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.set('view engine', 'ejs');

mongoose.connect(process.env.MLAB_CONNSTR)
.then(() => console.log('Database connected successfully'))
.catch(err => console.log(err.message));

app.get('/', (req, res) => res.render('index'));
require('./routes/bot.routes')(app);
require('./routes/webhook.routes')(app);
require('./routes/assuntos.routes')(app);
require('./routes/cursos.routes')(app);
require('./routes/documentos.routes')(app);
require('./routes/eventos.routes')(app);
require('./routes/pessoas.routes')(app);
require('./routes/servicos.routes')(app);

app.listen(port, () => console.log(`listening on http://localhost:${port}`));