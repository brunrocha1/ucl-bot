'use strict';

const Assunto = require('../models/assuntos.model');

exports.create = async (req, res) => {
    let assunto = new Assunto(req.body);
    
    try {
        let result = await assunto.save();
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.findAll = async (req, res) => {
    let query = {};

    try{
        let result = await Assunto.find(query);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.findOne = async (req, res) => {
    try {
        let result = await Assunto.findById(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.update = async (req, res) => {
    try {
        let result = await Assunto.findByIdAndUpdate(req.params.id, req.body);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.delete = async (req, res) => {
    try {
        let result = await Assunto.findByIdAndDelete(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}