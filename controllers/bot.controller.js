'use strict';

const { Suggestion } = require('dialogflow-fulfillment');
const apiai = require('apiai');
const bot = apiai(process.env.API_AI_CLIENT_ACCESS_TOKEN);
const messages = require('../messages/bot.message');

exports.sendMensage = (req, res) => {
    let message = req.body.message;
    let session = req.body.sessionId;
    let request = bot.textRequest(message, { sessionId: session });
    
    request.on('response', (response) => {
        res.send(response.result.fulfillment.messages);
    });
     
    request.on('error', (error) => res.send(error));
    
    request.end();
}

exports.sendSuggestions = (agent, index, result, title) => {
    if(result.opcoes.length === 1 && index !== 0) {
        agent.add(messages.NOVA_DUVIDA);
    } else {
        for(let reply of result.opcoes){
            if(reply.index !== index) {
                switch(reply.index) {
                    case 1: 
                        agent.add(new Suggestion({ title: title, reply: reply.texto }));
                        break;
                    case 2: 
                        if(result.funcionamento !== null && result.funcionamento !== undefined){
                            agent.add(new Suggestion({ title: title, reply: reply.texto }));
                        }
                        break;
                    case 3: 
                        if(result.local !== null && result.local !== undefined){
                            agent.add(new Suggestion({ title: title, reply: reply.texto }));
                        }
                        break;
                    case 4:
                        if(result.responsaveis !== null && result.responsaveis !== undefined){
                            agent.add(new Suggestion({ title: title, reply: reply.texto }));
                        }
                        break;
                    case 5:
                        if(result.requisito !== null && result.requisito !== undefined){
                            agent.add(new Suggestion({ title: title, reply: reply.texto }));
                        }
                        break;
                    case 6:
                        if(result.custo !== null && result.custo !== undefined){
                            agent.add(new Suggestion({ title: title, reply: reply.texto }));
                        }
                        break;
                    case 7:
                        if(result.prazo !== null && result.prazo !== undefined){
                            agent.add(new Suggestion({ title: title, reply: reply.texto }));
                        }
                        break;
                    case 8:
                        if(result.horario !== null && result.horario !== undefined){
                            agent.add(new Suggestion({ title: title, reply: reply.texto }));
                        }
                        break;
                    case 9:
                        if(result.formacao !== null && result.formacao !== undefined){
                            agent.add(new Suggestion({ title: title, reply: reply.texto }));
                        }
                        break;
                    case 10:
                        if(result.email !== null && result.email !== undefined){
                            agent.add(new Suggestion({ title: title, reply: reply.texto }));
                        }
                        break;
                    case 11:
                        if(result.beneficios !== null && result.beneficios !== undefined){
                            agent.add(new Suggestion({ title: title, reply: reply.texto }));
                        }
                        break;
                    case 12:
                        if(result.grade !== null && result.grade !== undefined){
                            agent.add(new Suggestion({ title: title, reply: reply.texto }));
                        }
                        break;
                }
            }
        }
    }
}