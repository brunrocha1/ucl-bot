'use strict';

const Curso = require('../models/cursos.model');

exports.create = async (req, res) => {
    let curso = new Curso(req.body);
    
    try {
        let result = await curso.save();
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.findAll = async (req, res) => {
    let query = {};

    try{
        let result = await Curso.find(query);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.findOne = async (req, res) => {
    try {
        let result = await Curso.findById(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.update = async (req, res) => {
    try {
        let result = await Curso.findByIdAndUpdate(req.params.id, req.body);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.delete = async (req, res) => {
    try {
        let result = await Curso.findByIdAndDelete(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}