'use strict';

const Documento = require('../models/documentos.model');

exports.create = async (req, res) => {
    let documento = new Documento(req.body);

    try {
        let result = await documento.save()
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.findAll = async (req, res) => {
    let query = {};
    req.query.nome ? query.nome = req.query.nome : '';    
    
    try {
        let result = await Documento.find(query)
        res.status(200).json(result);
    } catch (err){
        res.status(500).json({
            message: err.message
        });
    }
}

exports.findOne = async (req, res) => {
    try {
        let result = await Documento.findById(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.update = async (req, res) => {
    try {
        let result = await Documento.findByIdAndUpdate(req.params.id, req.body);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.delete = async (req, res) => {
    try {
        let result = await Documento.findByIdAndDelete(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}