'use strict';

const Evento = require('../models/eventos.model');

exports.create = (req, res) => {
    let evento = new Evento(req.body);

    evento.save().then(result => {
        res.status(200).json(result);
    }).catch(err => {
        res.status(500).json({
            message: err.message
        });
    });
}

exports.findAll = (req, res) => {
    Evento.find().then(result => {
        res.status(200).json(result);
    }).catch(err => {
        res.status(500).json({
            message: err.message
        });
    });
}

exports.findOne = async (req, res) => {
    try {
        let result = await Evento.findById(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.update = async (req, res) => {
    try {
        let result = await Evento.findByIdAndUpdate(req.params.id, req.body);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.delete = async (req, res) => {
    try {
        let result = await Evento.findByIdAndDelete(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}