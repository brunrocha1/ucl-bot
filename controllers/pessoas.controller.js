'use strict';

const Pessoa = require('../models/pessoas.model');

exports.create = async (req, res) => {
    let pessoa = new Pessoa(req.body);

    try{
        let result = await pessoa.save();
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.findAll = async (req, res) => {
    let query = {};
    req.query.nome ? query.nome = req.query.nome : '';
    req.query.sobrenome ? query.sobrenome = req.query.sobrenome : '';
    req.query.cargo ? query.cargo = req.query.cargo : '';
    req.query.curso ? query.curso = req.query.curso : '';
    req.query.genero ? query.genero = req.query.genero : '';

    try {
        let result = await Pessoa.find(query);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.findOne = async (req, res) => {
    try {
        let result = await Pessoa.findById(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.update = async (req, res) => {
    try {
        let result = await Pessoa.findByIdAndUpdate(req.params.id, req.body);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.delete = async (req, res) => {
    try {
        let result = await Pessoa.findByIdAndDelete(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}