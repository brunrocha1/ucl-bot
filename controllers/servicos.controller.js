'use strict';

const Servico = require('../models/servicos.model');

exports.create = async (req, res) => {
    let servico = new Servico(req.body);

    try{
        let result = await servico.save();
        res.status(200).json(result);
    } catch(err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.findAll = async (req, res) => {
    let query = {};

    try{
        let result = await Servico.find(query);
        res.status(200).json(result);
    } catch(err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.findOne = async (req, res) => {
    try {
        let result = await Servico.findById(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.update = async (req, res) => {
    try {
        let result = await Servico.findByIdAndUpdate(req.params.id, req.body);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}

exports.delete = async (req, res) => {
    try {
        let result = await Servico.findByIdAndDelete(req.params.id);
        res.status(200).json(result);
    } catch (err) {
        res.status(500).json({
            message: err.message
        });
    }
}