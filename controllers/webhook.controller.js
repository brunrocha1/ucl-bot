'use strict';

const { WebhookClient } = require('dialogflow-fulfillment');

const assuntos = require('../intents/assuntos.intent');
const cursos = require('../intents/cursos.intent');
const documentos = require('../intents/documentos.intent');
const eventos = require('../intents/eventos.intent');
const lista = require('../intents/lista.intent');
const pessoas = require('../intents/pessoas.intent');
const pesquisa = require('../intents/pesquisa.intent');
const servicos = require('../intents/servicos.intent');

exports.webhookProcessing = (req, res) => {
    const agent = new WebhookClient({request: req, response: res});

    let intentMap = new Map();
    intentMap.set('Assuntos', assuntos);
    intentMap.set('Assuntos.continuacao', assuntos);
    intentMap.set('Cursos', cursos);
    intentMap.set('Cursos.continuacao', cursos);
    intentMap.set('Documentos', documentos);
    intentMap.set('Documentos.continuacao', documentos);
    intentMap.set('Eventos', eventos);
    intentMap.set('Eventos.continuacao', eventos);
    intentMap.set('lista.intent', lista);
    intentMap.set('Pessoas', pessoas);
    intentMap.set('Pessoas.continuacao', pessoas);
    intentMap.set('Pesquisa', pesquisa);
    intentMap.set('Servicos', servicos);
    intentMap.set('Servicos.continuacao', servicos);
    agent.handleRequest(intentMap);
}