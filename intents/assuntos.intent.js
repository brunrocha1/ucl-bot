'use strict';

const { Suggestion } = require('dialogflow-fulfillment');
const Assunto = require('../models/assuntos.model');
const messages = require('../messages/bot.message');
const bot = require('../controllers/bot.controller');

module.exports = async (agent) => {
    let assunto = agent.parameters.assunto || agent.contexts[0].parameters.assunto;
    let interesse = agent.parameters.interesse || agent.contexts[0].parameters.interesse;

    let result = await Assunto.findOne({ nome: assunto });
    if(result){
        switch(interesse){
            case 'descricao':
                agent.add(result.descricao);
                bot.sendSuggestions(agent, 1, result, messages.getTitulo(result.nome));
                break;
            case 'funcionamento':
                if(result.funcionamento !== null && result.funcionamento !== undefined) {
                    agent.add(result.funcionamento);
                    bot.sendSuggestions(agent, 2, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
            case 'local':
                if (result.local !== null && result.local !== undefined) {
                    agent.add(result.local);
                    bot.sendSuggestions(agent, 3, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
            case 'responsavel':
                if (result.responsaveis !== null && result.responsaveis !== undefined) {
                    agent.add(result.responsaveis);
                    bot.sendSuggestions(agent, 4, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
            case 'custo':
                if (result.custo !== null && result.custo !== undefined) {
                    agent.add(result.custo);
                    bot.sendSuggestions(agent, 6, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
            case 'prazo':
                if (result.prazo !== null && result.prazo !== undefined) {
                    agent.add(result.prazo);
                    bot.sendSuggestions(agent, 7, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
            case 'requisito':
                if (result.requisito !== null && result.requisito !== undefined) {
                    agent.add(result.requisito);
                    bot.sendSuggestions(agent, 5, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
            case 'horario':
                if (result.horario !== null && result.horario !== undefined) {
                    agent.add(result.horario);
                    bot.sendSuggestions(agent, 8, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
            case 'beneficios':
                if (result.beneficios !== null && result.beneficios !== undefined) {
                    agent.add(result.beneficios);
                    bot.sendSuggestions(agent, 11, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
            default:
                if(agent.parameters.interesse === '' 
                || agent.parameters.interesse === null
                || agent.parameters.interesse === undefined) {
                    agent.add(result.descricao);
                    bot.sendSuggestions(agent, 1, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
        }
    }
}