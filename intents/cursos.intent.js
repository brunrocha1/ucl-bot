'use strict';

const { Suggestion } = require('dialogflow-fulfillment');
const Curso = require('../models/cursos.model');
const messages = require('../messages/bot.message');
const bot = require('../controllers/bot.controller');

module.exports = async (agent) => {
    let curso = agent.parameters.curso || agent.contexts[0].parameters.curso;
    let interesse = agent.parameters.interesse || agent.contexts[0].parameters.interesse;

    let result = await Curso.findOne({ nome: curso });
    if(result){
        switch(interesse){
            case 'descricao':
                for(let descricao of result.descricao){
                    agent.add(descricao);
                }
                bot.sendSuggestions(agent, 1, result, messages.getTitulo(result.nome));
                break;
            case 'funcionamento':
                if(result.funcionamento !== null && result.funcionamento !== undefined) {
                    agent.add(result.funcionamento);
                    bot.sendSuggestions(agent, 2, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
            case 'responsavel':
                if (result.responsaveis !== null && result.responsaveis !== undefined) {
                    agent.add(result.responsaveis);
                    bot.sendSuggestions(agent, 4, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
            case 'custo':
                if (result.custo !== null && result.custo !== undefined) {
                    agent.add(result.custo);
                    bot.sendSuggestions(agent, 6, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
            case 'grade':
                if (result.grade !== null && result.grade !== undefined) {
                    agent.add(result.grade);
                    bot.sendSuggestions(agent, 12, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
            default:
                if(agent.parameters.interesse === '' 
                || agent.parameters.interesse === null
                || agent.parameters.interesse === undefined) {
                    agent.add(result.descricao);
                    bot.sendSuggestions(agent, 1, result, messages.getTitulo(result.nome));
                } else {
                    agent.add(messages.FALLBACK);
                    bot.sendSuggestions(agent, 0, result, messages.getTituloFallback(result.nome));
                }
                break;
        }
    }
}