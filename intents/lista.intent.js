'use strict';

const { Suggestion } = require('dialogflow-fulfillment');
const Assunto = require('../models/assuntos.model');
const Curso = require('../models/cursos.model');
const Documento = require('../models/documentos.model');
const Evento = require('../models/eventos.model');
const Pessoa = require('../models/pessoas.model');
const Servico = require('../models/servicos.model');
const mensagens = require('../messages/lista.message');

module.exports = async (agent) => {
    let intent = agent.parameters.intent;
    let categoria = agent.parameters.categoria;

    if(categoria !== undefined && categoria !== '' && intent === ''){
        intent = 'cursos';
    }

    let results;

    try{
        switch(intent){
            case 'assuntos':
                results = await Assunto.find().select('nome -_id');
                if(results.length !== 0){
                    for(let result of results){
                        agent.add(new Suggestion({
                            title: mensagens.TITULO_ASSUNTOS,
                            reply: result.nome
                        })); 
                    }
                } else {
                    agent.add(mensagens.ITEM_NAO_ENCONTRADO('m', 'assunto'));
                }
                break;
            case 'cursos':
                let query = {};
                if(agent.parameters.categoria !== undefined && agent.parameters.categoria !== '')
                    query.categoria = agent.parameters.categoria; 
                
                results = await Curso.find(query).select('nome -_id');
                if(results.length !== 0){
                    for(let result of results){
                        agent.add(new Suggestion({
                            title: mensagens.TITULO_CURSOS,
                            reply: result.nome
                        }));  
                    }
                } else {
                    if(agent.parameters.categoria !== undefined) {
                        agent.add(mensagens.ITEM_NAO_ENCONTRADO('m', 'curso'));
                    } else {
                        agent.add(mensagens.CATEGORIA_NAO_ENCONTRADA);
                    }
                }
                break;
            case 'categorias':
                results = await Curso.find().distinct('categoria');
                
                if(results.length !== 0){
                    for(let result of results){
                        if(result !== ''){
                            agent.add(new Suggestion({
                                title: mensagens.TITULO_CATEGORIAS,
                                reply: result
                            }));  
                        }
                    }
                } else {
                    agent.add(mensagens.ITEM_NAO_ENCONTRADO('m', 'categoria'));
                }
                break;
            case 'documentos':
                results = await Documento.find().select('nome -_id');
                if(results.length !== 0){
                    for(let result of results){
                        agent.add(new Suggestion({
                            title: mensagens.TITULO_DOCUMENTOS,
                            reply: result.nome
                        }));  
                    }
                } else {
                    agent.add(mensagens.ITEM_NAO_ENCONTRADO('m', 'documento'));
                }
                break;
            case 'eventos':
                results = await Evento.find().select('nome -_id');
                if(results.length !== 0){
                    for(let result of results){
                        agent.add(new Suggestion({
                            title: mensagens.TITULO_EVENTOS,
                            reply: result.nome
                        })); 
                    }
                } else {
                    agent.add(mensagens.ITEM_NAO_ENCONTRADO('m', 'evento'));
                }
                break;
            case 'pessoas':
                results = await Pessoa.find().select({"nome": 1, "sobrenome": 2});
                if(results.length !== 0){
                    for(let result of results){
                        agent.add(new Suggestion({
                            title: mensagens.TITULO_PESSOAS,
                            reply: `${result.nome} ${result.sobrenome}`
                        })); 
                    }
                } else {
                    agent.add(mensagens.ITEM_NAO_ENCONTRADO('f', 'pessoa'));
                }
                break;
            case 'servicos':
                results = await Servico.find().select('nome -_id');
                if(results.length !== 0){
                    for(let result of results){
                        agent.add(new Suggestion({
                            title: mensagens.TITULO_SERVICOS,
                            reply: result.nome
                        })); 
                    }
                } else {
                    agent.add(mensagens.ITEM_NAO_ENCONTRADO('m', 'serviço'));
                }
                break;
        }
    } catch (err) {
        console.log(err);
    }
}