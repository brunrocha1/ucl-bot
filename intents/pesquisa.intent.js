'use strict';

const { Suggestion } = require('dialogflow-fulfillment');
const Pesquisa = require('../models/pesquisa.model');
const messages = require('../messages/pesquisa.message');

module.exports = async (agent) => {
    let resposta1 = agent.parameters.resposta1;
    let resposta2 = agent.parameters.resposta2;
    let resposta3 = agent.parameters.resposta3;
    let resposta4 = agent.parameters.resposta4;
    let resposta5 = agent.parameters.resposta5;
    let resposta6 = agent.parameters.resposta6;

    try{
        if(!resposta1){
            let pergunta1 = new Suggestion({
                title: messages.PERGUNTA1,
                reply: 'Não'
            });
            pergunta1.addReply_('Sim');

            agent.add(messages.CONTEXTO_PERGUNTA1);
            agent.add(pergunta1);
        } else if(!resposta2){
            let pergunta2 = new Suggestion({
                title: messages.PERGUNTA2,
                reply: '1 - De forma nenhuma'
            });
            pergunta2.addReply_('2 - Não, só algumas');
            pergunta2.addReply_('3 - Parcialmente');
            pergunta2.addReply_('4 - Sim, muitas');
            pergunta2.addReply_('5 - Totalmente');

            agent.add(messages.CONTEXTO_PERGUNTA2);
            agent.add(pergunta2);
        } else if(!resposta3){
            let pergunta3 = new Suggestion({
                title: messages.PERGUNTA3,
                reply: '1 - Muito difícil'
            });
            pergunta3.addReply_('2 - Difícil');
            pergunta3.addReply_('3 - Normal');
            pergunta3.addReply_('4 - Fácil');
            pergunta3.addReply_('5 - Muito Fácil');

            agent.add(messages.CONTEXTO_PERGUNTA3);
            agent.add(pergunta3);
        } else if(!resposta4){
            let pergunta4 = new Suggestion({
                title: messages.PERGUNTA4,
                reply: 'Não'
            });
            pergunta4.addReply_('Sim');

            agent.add(pergunta4);
        } else if(!resposta5){
            let pergunta5 = new Suggestion({
                title: messages.PERGUNTA5,
                reply: 'Não'
            });
            pergunta5.addReply_('Sim');

            agent.add(messages.CONTEXTO_PERGUNTA5);
            agent.add(pergunta5);
        } else if(!resposta6){
            agent.add(messages.PERGUNTA6);
        } else {
            let pesquisa = new Pesquisa(agent.parameters);
            await pesquisa.save();
            agent.add(messages.AGRADECIMENTO);
        }        
    } catch (err){
        console.log(err);
    }
}