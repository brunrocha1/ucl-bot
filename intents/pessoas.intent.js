'use strict';

const { Suggestion } = require('dialogflow-fulfillment');
const Pessoas = require('../models/pessoas.model');
const messages = require('../messages/bot.message');
const bot = require('../controllers/bot.controller');

module.exports = async (agent) => {
    let interesse = agent.parameters.interesse || agent.contexts[0].parameters.interesse;
    let query = {};
    agent.parameters.nome || agent.contexts[0].parameters.nome ? 
    query.nome = agent.parameters.nome || agent.contexts[0].parameters.nome : '';
    agent.parameters.sobrenome || agent.contexts[0].parameters.sobrenome ? 
    query.sobrenome = agent.parameters.sobrenome || agent.contexts[0].parameters.sobrenome : '';
    agent.parameters.cargo || agent.contexts[0].parameters.cargo ? 
    query.cargo = agent.parameters.cargo || agent.contexts[0].parameters.cargo : '';

    try{
        let result = await Pessoas.find(query);
        if(result) {
            if(result.length === 1) {
                switch (interesse) {
                    case 'descricao': 
                        if(result[0].descricao !== null && result[0].descricao !== undefined){
                            agent.add(result[0].descricao);
                            bot.sendSuggestions(agent, 1, result[0], messages.getTitulo(result[0].nome));
                        } else {
                            agent.add(messages.FALLBACK);
                            bot.sendSuggestions(agent, 0, result[0], messages.getTituloFallback(result[0].nome));
                        }
                        break;
                    case 'formacao':
                        if(result[0].formacao !== null && result[0].formacao !== undefined){
                            agent.add(result[0].formacao);
                            bot.sendSuggestions(agent, 9, result[0], messages.getTitulo(result[0].nome));
                        } else {
                            agent.add(messages.FALLBACK);
                            bot.sendSuggestions(agent, 0, result[0], messages.getTituloFallback(result[0].nome));
                        }
                        break;
                    case 'contato':
                        if(result[0].email !== null && result[0].email !== undefined){
                            agent.add(result[0].email);
                            bot.sendSuggestions(agent, 10, result[0], messages.getTitulo(result[0].nome));
                        } else {
                            agent.add(messages.FALLBACK);
                            bot.sendSuggestions(agent, 0, result[0], messages.getTituloFallback(result[0].nome));
                        }
                        break;
                    default:
                        if(agent.parameters.interesse === '' 
                        || agent.parameters.interesse === null
                        || agent.parameters.interesse === undefined) {
                            agent.add(result[0].descricao);
                            bot.sendSuggestions(agent, 1, result[0], messages.getTitulo(result[0].nome));
                        } else {
                            agent.add(messages.FALLBACK);
                            bot.sendSuggestions(agent, 0, result[0], messages.getTituloFallback(result[0].nome));
                        }
                        break;
                }
            } else if (result.length > 1) {
                agent.add(`Sobre qual ${agent.parameters.nome} você gostaria de saber?`);
                for(let r of result){
                    await agent.add(new Suggestion(`${r.nome} ${r.sobrenome}`));
                }
            }
        }
    } catch (err) {
        console.log(err);
    }
}