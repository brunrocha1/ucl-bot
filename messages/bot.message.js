"use strict";

module.exports = {
    NOVA_DUVIDA: "O que mais você gostaria de saber?",
    FALLBACK: "Huum, eu ainda não tenho essa informação, mas talvez eu possa te ajudar.",
    getTitulo: nome => {
        return `Se quiser saber mais sobre '${nome}', você pode selecionar uma opção abaixo ou escrever uma nova pergunta.`
    },
    getTituloFallback: nome => {
        return `Essas são as opções do que eu sei sobre ${nome}:`;
    } 
}