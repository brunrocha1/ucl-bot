"use strict";

module.exports = {
    TITULO_ASSUNTOS: "Esses são os 'assuntos' que eu posso te ajudar:",
    TITULO_CURSOS: "Sobre qual 'curso' você quer saber?",
    TITULO_CATEGORIAS: "Essas são as 'categorias' que eu tenho informação:",
    TITULO_DOCUMENTOS: "Esses são os 'documentos' que eu tenho informação:",
    TITULO_EVENTOS: "Esses são os 'eventos' que eu conheço:",
    TITULO_PESSOAS: "Eu tenho 'informações' sobre essas pessoas:",
    TITULO_SERVICOS: "Eu consigo falar sobre esses 'serviços':",
    CATEGORIA_NAO_ENCONTRADA: "Descula, eu não sei nada relacionado a categoria registrada. Se desejar, você pode escrever 'lista de categorias', ou 'categorias' e eu mostro as categorias que eu conheço para você.",
    ITEM_NAO_ENCONTRADO: (genero, item) => { 
        if(genero === "m")
            return `Desculpa, mas eu ainda não sei falar sobre nenhum '${item}'. :/`;
        else if(gennero === "f")
            return `Desculpa, mas eu ainda não sei falar sobre nenhuma '${item}'. :/`;
        
    }
}