'use strict';

module.exports = {
    PERGUNTA1: 'Você já havia utilizando um chatbot antes?',
    PERGUNTA2: 'Então me fala, de 1 a 5, eu consegui responder bem a suas perguntas?',
    PERGUNTA3: 'Assim, de 1 a 5, qual foi o nivel de dificuldade que você teve para me utilizar?',
    PERGUNTA4: 'E você gostou da experiência de utilizar um assistênte virtual?',
    PERGUNTA5: 'Você acha que essa é uma ideia boa?',
    PERGUNTA6: 'E você teria alguma sugestão de melhoria para mim?',
    CONTEXTO_PERGUNTA1: 'Assistêntes virtuais, ou chatbots, são robôs criados para responder a perguntas automaticamente de forma independente.',
    CONTEXTO_PERGUNTA2: 'Ótimo!! Como eu ainda estou sendo treinado, ainda não conheço tudo o que eu preciso para tirar todas as suas dúvidas.',
    CONTEXTO_PERGUNTA3: 'Normalmente as pessoas tem um pouco de dificuldade em tirar dúvidas quando falam comigo na primeira vez.',
    CONTEXTO_PERGUNTA5: 'Bom, a ideia é que eu seja um local unico onde você possa tirar todas as suas dúvidas relacionadas a faculdade.',
    AGRADECIMENTO: 'A pesquisa foi finalizada. Muito obrigado pela sua participação.'
}