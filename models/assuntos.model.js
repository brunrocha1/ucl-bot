'use strict';

const mongoose = require('mongoose');

const assuntoSchema = mongoose.Schema({
    nome: { type: String, require: true },
    descricao: { type: String, require: true },
    funcionamento: { type: String },
    local: { type: String },
    responsaveis: { type: String },
    custo: { type: String },
    requisito: { type: String },
    prazo: { type: String },
    horario: { type: String },
    beneficios: { type: String },
    opcoes: [{ index: Number, texto: String }],
},{
    timestamps: true
});

module.exports = mongoose.model('Assunto', assuntoSchema);