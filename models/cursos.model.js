'use strict';

const mongoose = require('mongoose');

const cursoSchema = mongoose.Schema({
    nome: { type: String, require: true },
    categoria: { type: String, require: true },
    descricao: [{ type: String, require: true }],
    funcionamento: { type: String },
    responsaveis: { type: String },
    custo: { type: String },
    grade: { type: String },
    imagens: [{type: String}],
    opcoes: [{ index: Number, texto: String }],
},{
    timestamps: true
});

module.exports = mongoose.model('Curso', cursoSchema);