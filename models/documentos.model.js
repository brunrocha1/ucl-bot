'use strict';

const mongoose = require('mongoose');

const documentoSchema = mongoose.Schema({
    nome: { type: String, require: true },
    descricao: { type: String, require: true },
    requisito: { type: String },
    custo: { type: String },
    local: { type: String },
    prazo: { type: String },
    opcoes: [{ index: Number, texto: String }],
},{
    timestamps: true
});

module.exports = mongoose.model('Documento', documentoSchema);