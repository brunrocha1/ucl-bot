'use strict';

const mongoose = require('mongoose');

const pesquisaSchema = mongoose.Schema({
    resposta1: { type: String, required: true },
    resposta2: { type: String, required: true },
    resposta3: { type: String, required: true },
    resposta4: { type: String, required: true },
    resposta5: { type: String, required: true },
    resposta6: { type: String, required: true },
},{
    timestamps: true
});

module.exports = mongoose.model('Pesquisa', pesquisaSchema);