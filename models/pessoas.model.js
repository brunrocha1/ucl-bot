'use strict';

const mongoose = require('mongoose');

const pessoaSchema = mongoose.Schema({
    nome: { type: String, require: true },
    sobrenome: { type: String, require: true },
    cargo: { type: String, require: true },
    descricao: { type: String, require: true },
    formacao: { type: String },
    imagem: { type: String },
    email: { type: String },
    opcoes: [{ index: Number, texto: String }],
}, {
    timestamps: true
});

module.exports = mongoose.model('Pessoa', pessoaSchema);