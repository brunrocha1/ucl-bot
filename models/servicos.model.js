'use strict';

const mongoose = require('mongoose');

const servicoSchema = mongoose.Schema({
    nome: { type: String, require: true },
    descricao: { type: String, require: true },
    horario: { type: String },
    funcionamento: { type: String },
    local: { type: String },
    responsaveis: { type: String },
    requisito: { type: String },
    opcoes: [{ index: Number, texto: String }],
},{
    timestamps: true
});

module.exports = mongoose.model('Servico', servicoSchema);