'use strict';

let chat = document.querySelector('div.container');
let sendMessage = document.getElementById('sendMessage');
let message = document.getElementById('message');

let formatMessage = (message) => {
    let result = document.createElement('p');
    let texts = message.split("'");
    for(let i = 0; i < texts.length; i++){
        if(i%2 !== 0){
            let strong = document.createElement('strong');
            strong.appendChild(document.createTextNode(texts[i]))
            result.appendChild(strong);
        } else {
            result.appendChild(document.createTextNode(texts[i]));
        }
    }
    return result;
}

let getBotProfile = () => {
    let h5 = document.createElement('h5');
    h5.setAttribute('class', 'profile-aligned');
    let i = document.createElement('i');
    i.setAttribute('class', 'ui circular user teal icon');
    h5.appendChild(i);
    h5.appendChild(document.createTextNode('Assistente Virtual da Ucl:'));
    return h5;
}

let getUserProfile = () => {
    let h5 = document.createElement('h5');
    h5.setAttribute('class', 'profile-aligned');
    let i = document.createElement('i');
    i.setAttribute('class', 'ui circular user icon');
    h5.appendChild(i);
    h5.appendChild(document.createTextNode('Você:'));
    return h5;
}

let setUserMessage = (message) => {
    if(chat.lastElementChild.classList.contains('teal')){
        chat.lastElementChild.remove();
    }
    let segment = document.createElement('div');
    segment.setAttribute('class', 'ui teal inverted padded segment right floated');
    let text = formatMessage(message); 
    if(chat.lastElementChild === null 
        || !chat.lastElementChild.classList.contains('teal')) {
            segment.appendChild(getUserProfile());
        }
    segment.appendChild(text);
    chat.appendChild(segment);
    scrolldown(chat);
};

let createMessage = (message) => {
    let segment = document.createElement('div');
    segment.setAttribute('class', 'ui padded segment floated');
    let text = formatMessage(message);
    if(chat.lastElementChild === null 
        || chat.lastElementChild.classList.contains('teal')) {
            segment.appendChild(getBotProfile());
        }
    segment.appendChild(text);
    chat.appendChild(segment);
    scrolldown(chat);
};

let sendSelectedMessage = (event) =>{
    let button = event.target || event.scrElement;
    let dialogBox = button.parentElement.parentElement;
    button.parentElement.remove();
    let text = document.createElement('p');
    text.appendChild(document.createTextNode(button.textContent));
    dialogBox.appendChild(text);
    postMessage(button.textContent);
}

let createCardMessage = (message) => {};
let createQuestionMessage = (message) => {
    if(message.title){
        createMessage(message.title);
    }
    let segment = document.createElement('div');
    segment.setAttribute('class', 'ui teal inverted padded segment right floated');
    let replies = document.createElement('div');
    for(let reply of message.replies){
        let button = document.createElement('button');
        button.setAttribute('class', 'ui inverted large button');
        button.appendChild(document.createTextNode(reply));
        button.addEventListener('click', sendSelectedMessage);
        replies.appendChild(button);
    }
    if(chat.lastElementChild === null 
        || !chat.lastElementChild.classList.contains('teal')) {
            segment.appendChild(getUserProfile());
        }
    segment.appendChild(replies);
    chat.appendChild(segment);
    scrolldown(chat);
};
let createImageMessage = (message) => {};

let setBotMessage = (messages) => {
    for (let message of messages) {
        switch (message.type) {
            case 0:
                if(message.speech !== ''){
                    createMessage(message.speech);
                }
                break;
            case 1:
                createCardMessage(message);
                break;
            case 2:
                createQuestionMessage(message);
                break;
            case 3:
                createImageMessage(message);
            default:
                console.log(message);
                createMessage('Opss... parece que tivemos algum problema.');
                break;
        }
    }
};

let postMessage = (text) => {
    let data = {
        sessionId: getSessionId(),
        message: text
    }
    $.ajax({
      type: "POST",
      url: '/dialogflow/ucl-bot',
      data: data,
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      success: function(response){
          setBotMessage(response);
      }
    });
};