'use strict'

let scrolldown = (div) => {
    div.scroll(0, div.scrollHeight);
}

let guid = () => {
    let s4 = () => {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

let getSessionId = () => {
    if(sessionStorage.length === 0){
        sessionStorage.setItem('session', guid());
    }
    return sessionStorage.getItem('session');
}
