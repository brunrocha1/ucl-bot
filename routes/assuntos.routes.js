'use strict';

const assuntosController = require('../controllers/assuntos.controller');

module.exports = (app) => {
    app.route('/assuntos')
        .get(assuntosController.findAll)
        .post(assuntosController.create);
    app.route('/assuntos/:id')
        .get(assuntosController.findOne)
        .put(assuntosController.update)
        .delete(assuntosController.delete);
}