'use strict';

const bot = require('../controllers/bot.controller');

module.exports = (app) => {
    app.post('/dialogflow/ucl-bot', bot.sendMensage);
}