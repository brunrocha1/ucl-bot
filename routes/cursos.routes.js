'use strict';

const cursosController = require('../controllers/cursos.controller');

module.exports = (app) => {
    app.route('/cursos')
        .get(cursosController.findAll)
        .post(cursosController.create);
    app.route('/cursos/:id')
        .get(cursosController.findOne)
        .put(cursosController.update)
        .delete(cursosController.delete);
}