'use strict';

const documentosController = require('../controllers/documentos.controller');

module.exports = (app) => {
    app.route('/documentos')
        .get(documentosController.findAll)
        .post(documentosController.create);
    app.route('/documentos/:id')
        .get(documentosController.findOne)
        .put(documentosController.update)
        .delete(documentosController.delete);
}