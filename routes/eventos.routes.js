'use strict';

const eventosController = require('../controllers/eventos.controller');

module.exports = (app) => {
    app.route('/eventos')
        .get(eventosController.findAll)
        .post(eventosController.create);
     app.route('/eventos/:id')
        .get(eventosController.findOne)
        .put(eventosController.update)
        .delete(eventosController.delete);
}