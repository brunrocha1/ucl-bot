'use strict';

const pessoasController = require('../controllers/pessoas.controller');

module.exports = (app) => {
    app.route('/pessoas')
        .get(pessoasController.findAll)
        .post(pessoasController.create);
    app.route('/pessoas/:id')
        .get(pessoasController.findOne)
        .put(pessoasController.update)
        .delete(pessoasController.delete);
}