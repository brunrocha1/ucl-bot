'use strict';

const servicosController = require('../controllers/servicos.controller');

module.exports = (app) => {
    app.route('/servicos')
        .get(servicosController.findAll)
        .post(servicosController.create);
    app.route('/servicos/:id')
        .get(servicosController.findOne)
        .put(servicosController.update)
        .delete(servicosController.delete);
}