'use strick';

let webhookController = require('../controllers/webhook.controller');

module.exports = (app) => {
    app.post('/webhook', webhookController.webhookProcessing);
}